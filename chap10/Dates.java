import java.util.Calendar;

public class Dates {
    public static void main(String [] args){
        Calendar calendar = Calendar.getInstance();
        String todaysDate = String.format("%tc", calendar);
        System.out.println(todaysDate);
        String todaysTime = String.format("%tr", calendar);
        System.out.println(todaysTime);
        String dayOfTheWeek = String.format("%tA", calendar);
        System.out.println(dayOfTheWeek);
        String month = String.format("%tB", calendar);
        System.out.println(month);
        String day = String.format("%tD", calendar);
        System.out.println(day);
        String weekMonthDay = String.format("%tA, %<tB %<td", calendar);
        System.out.println(weekMonthDay);
    }
}
