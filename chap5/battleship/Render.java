import java.util.ArrayList;

class Render{
  static boolean renderMap(int width, ArrayList<ArrayList<ArrayList<Integer>>> arrayListBattleships,  ArrayList<ArrayList<Integer>> hitList, ArrayList<ArrayList<Integer>> missList){
    System.out.println(arrayListBattleships);
    for(int rowNumber=0; rowNumber<width; rowNumber++){
      System.out.print((char)(rowNumber + 65));
      //for displaying ships
      /*for(int colNumber=0; colNumber<width; colNumber++){
        boolean shipCoord = false;
        for(int battleShipNumber=0; battleShipNumber<arrayListBattleships.size(); battleShipNumber++){
          for(int coordNumber=0; coordNumber<arrayListBattleships.get(battleShipNumber).size(); coordNumber++){
            if((arrayListBattleships.get(battleShipNumber).get(coordNumber).get(0) == rowNumber) && (arrayListBattleships.get(battleShipNumber).get(coordNumber).get(1) == colNumber)){
              shipCoord = true;
            }
          }
        }
        if(shipCoord == true){
          System.out.print("|");
        }
        else{
          System.out.print("~");
        }
      }*/
      for(int colNumber=0; colNumber<width; colNumber++){
        ArrayList<Integer> coordinate = new ArrayList<>(0);
        coordinate.add(rowNumber);
        coordinate.add(colNumber);
        if(hitList.contains(coordinate)){
          System.out.print("X");
        }
        else if(missList.contains(coordinate)){
          System.out.print("O");
        }
        else{
          System.out.print("~");
        }
      }
      System.out.println("");
    }
    System.out.print(" ");
    for(int num=0; num<width; num++){System.out.print(num);}
    System.out.println();
    return true;
  }
}
