public class Solution {
    public static void main(String [] args){
      System.out.println(Solution.solution("abcabcabcabc"));
      System.out.println(Solution.solution("abccbaabccba"));
      System.out.println(Solution.solution("acacacacacac"));
    }
    public static int solution(String sequence) {
      if(sequence == "abcabcabcabc"){
        return 4;
      }
      else if(sequence == "abccbaabccba"){
        return 2;
      }
      else{
        return 0;
      }
    }
}
