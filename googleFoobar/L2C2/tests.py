import solution
from random import randint

randomlist = []
for i in range(0,99):
    randomlist.append(str(randint(0,9))+"."+str(randint(-1,9))+"."+str(randint(-1,9)))
print(randomlist)
print(solution.solution(randomlist))

print(["1.11", "2.0.0", "1.2", "2", "0.1", "1.2.1", "1.1.1", "2.0"])
print(solution.solution(["1.11", "2.0.0", "1.2", "2", "0.1", "1.2.1", "1.1.1", "2.0"]))
print(["1.1.2", "1.0", "1.3.3", "1.0.12", "1.0.2"])
print(solution.solution(["1.1.2", "1.0", "1.3.3", "1.0.12", "1.0.2"]))
#print(solution.solution(["1.1.2", "1.0.0", "1.3.3", "1.0.12", "1.0.2"]))
