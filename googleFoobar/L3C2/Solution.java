import java.util.ArrayList;
import java.util.Arrays;

public class Solution {
    public static int solution(int[][] inputMap) {
      ArrayList<ArrayList<Integer>> inputMapArrayList = new ArrayList<>(0);
      for(int rowNum=0; rowNum<inputMap.length; rowNum++){
        ArrayList<Integer> rowArrayList = new ArrayList<>(0);
        for(int colNum=0; colNum<inputMap[rowNum].length; colNum++){
          rowArrayList.add(inputMap[rowNum][colNum]);
        }
        inputMapArrayList.add(rowArrayList);
      }
      ArrayList<ArrayList<ArrayList<Integer>>> solutionArrayList = new ArrayList<>(0);
      solutionArrayList.add(inputMapArrayList);
      int maxRows = inputMap.length;
      int maxCollumns = inputMap[0].length;
      ArrayList<Integer> appendList = new ArrayList<>(0);
      appendList.addAll(Arrays.asList(0,0,0));
      solutionArrayList.get(0).add(appendList);

      int steps = 1;
      while(true){
        steps += 1;
        for(int mapNum=0; mapNum<solutionArrayList.size(); mapNum++){
          ArrayList<ArrayList<Integer>> map = solutionArrayList.remove(0);
          System.out.println(map.get(map.size()-1).get(1));
          System.out.println(map.get(map.size()-1).get(2));
          System.out.println(map.get(map.get(map.size()-1).get(1)).get(map.get(map.size()-1).get(2)));
          //map.get(map.get(map.size()-1).get(1)).get(map.get(map.size()-1).get(2)) = 2;
          map.get(0).get(0) = 2;
        }
        return maxCollumns;
      }

    }
}
